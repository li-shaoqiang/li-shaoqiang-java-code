package model;

/**
 * 
 * @author Li
 *
 */

public class Book {
	private String bookName;
	private String authorName;
	private String bookCode;
	private String price;
	private String bookAllNumber;
	private String location;
	private String lendDate;
	private String returnDate;

	public Book(String name, String authorName, String bookCode, String price, String bookAllNumber, String location) {
		super();
		this.bookName = name;
		this.authorName = authorName;
		this.bookCode = bookCode;
		this.price = price;
		this.bookAllNumber = bookAllNumber;
		this.location = location;
	}

	public Book() {
		this("no information", "no information", "no information", "no information", "no information",
				"no information");
	}

	public String getLendDate() {
		return lendDate;
	}

	public void setLendDate(String lendDate) {
		this.lendDate = lendDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getAllBookNumber() {
		return bookAllNumber;
	}

	public void setAllBookNumber(String bookAllNumber) {
		this.bookAllNumber = bookAllNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Book [bookName=" + bookName + ", authorName=" + authorName + ", bookCode=" + bookCode + ", location="
				+ location + ", lendDate=" + lendDate + ", returnDate=" + returnDate + "]";
	}

	public void print() {
		String checkString = "no information";
		if (this.bookName != checkString && this.bookAllNumber != checkString) {
			System.out.println("| 图书名称：" + this.bookName + " | 作者：" + this.authorName + " | 价格：" + this.price);
			System.out
					.println("| ISBN码：" + this.bookCode + " | 剩余数量：" + this.bookAllNumber + " | 存放处：" + this.location);
			System.out.println("|-----------------------------------------------------------------------|");
		}
	}
}
