package model;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import util.Library;

/**
 * 
 * @author Li
 *
 */

public class Student {
	List<Book> myBookList = new ArrayList<>();
	private String studentName;
	private String cardId;
	private String gender;
	private int maxLoanableNumber;
	private int borrowedNumber;

	public List<Book> getMyBookList() {
		return myBookList;
	}

	public void setMyBookList(ArrayList<Book> myBookList) {
		this.myBookList = myBookList;
	}

	public Student(String cardId, String studentName, String gender, int maxLoanableNumber, int borrowedNumber) {
		super();
		this.cardId = cardId;
		this.studentName = studentName;
		this.gender = gender;
		this.maxLoanableNumber = maxLoanableNumber;
		this.borrowedNumber = borrowedNumber;
	}

	public Student() {
		super();
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getMaxLoanableNumber() {
		return maxLoanableNumber;
	}

	public void setMaxLoanableNumber(int maxLoanableNumber) {
		this.maxLoanableNumber = maxLoanableNumber;
	}

	public int getBorrowedNumber() {
		return borrowedNumber;
	}

	public void setBorrowedNumber(int borrowedNumber) {
		this.borrowedNumber = borrowedNumber;
	}
    
	public void print() {
		System.out.println("| 您的个人信息如下：");
		System.out.println("| 图书证号:" + this.cardId + "  姓名:" + this.studentName + "  性别:" + this.gender);
		System.out.println("| 已借书" + this.borrowedNumber + "册 ，还可借书：" + this.maxLoanableNumber + "册");
	}

	public void printMyBookList() {
		for(int i=0;i<myBookList.size();i++) {
			System.out.println(myBookList.get(i).toString());
		}
	}
    
	public int searchInMyBookList(String bookName) {
		Iterator<Book> it = this.myBookList.iterator();
		Book targetBook = new Book();
		int n = 0;
		while (it.hasNext()) {
			targetBook = it.next();
			if (targetBook.getBookName().equals(bookName)) {
				break;
			}
			n++;
		}
		return n;
	}
	
	public boolean borrowOneBook(String bookName) {
		Iterator<Book> it = Library.getBookShelf().iterator();
		Book targetBook = new Book();
		int n = 0;
		boolean flag = true;
		while (it.hasNext()) {
			targetBook = it.next();
			if (targetBook.getBookName().equals(bookName)||targetBook.getBookCode().equals(bookName)) {
				break;
			}
			n++;
		}
		if (n<Library.getBookShelf().size()) {
			if (Integer.parseInt(targetBook.getAllBookNumber()) > 0) {
				if (this.getMaxLoanableNumber() > 0) {
					String borrowTime = Library.getBorrowTime();
					String returnTime = Library.getReturnTime();
					targetBook.setLendDate(borrowTime);
					targetBook.setReturnDate(returnTime);
					this.getMyBookList().add(targetBook);
					System.out.println("| 恭喜你借书成功！");
					Integer num1 = Integer.parseInt(Library.getBookShelf().get(n).getAllBookNumber());
					num1--;
					Library.getBookShelf().get(n).setAllBookNumber(num1.toString());;
					this.setMaxLoanableNumber(this.getMaxLoanableNumber() - 1);
					this.setBorrowedNumber(this.getBorrowedNumber() + 1);		
					System.out.println("| 你已借的书目如下：");
					this.printMyBookList();
				} else if (this.getMaxLoanableNumber() == 0) {
					System.out.println("| 抱歉，您已经不能再借书了！");
				}
			} else {
				System.out.println("| 抱歉，您想要的书已经没有了！");
			}
			System.out.println("| 是否继续借书(Y/N):");
			String flagString = Library.scanner.next();
			String checkString = "N";
			if (checkString.equals(flagString)) {
				flag = false;
			}
		}
		else {
			System.out.println("| 查无此书！请重新输入！");
		}
		return flag;
    }
	
	public boolean returnOneBook(String myBook) {
		int index1 = this.searchInMyBookList(myBook);
		int index2 =  Library.searchInBookshelf(myBook);
		boolean flag = true;
		if(index1<this.myBookList.size()) {
			this.myBookList.remove(index1);
			this.borrowedNumber--;
			this.maxLoanableNumber++;
			System.out.println("| 恭喜你还书成功！");
			Integer num = Integer.parseInt(Library.getBookShelf().get(index2).getAllBookNumber());
			num++;
			Library.getBookShelf().get(index2).setAllBookNumber(num.toString());
			System.out.println("| 你还有以下的书未还：");
			this.printMyBookList();
		}
		else {
			System.out.println("| 你没有借过此书！请重新输入！");
		}
		System.out.println("| 是否继续还书(Y/N):");
		String flagString = Library.scanner.next();
		String checkString = "N";
		if (checkString.equals(flagString)) {
			flag = false;
		}
		return flag;
	}
}
