package util;

import model.Student;
import ui.Ui;

public class Main {
	public static void main(String[] args) {
		Student nowStudent = Library.userLoginCheck();
		boolean flag = true;
		if(nowStudent.getCardId()!=null) {
			System.out.println("|                                                                       |");
			while(flag) {
				Ui.menu();
				flag = choose(nowStudent);
			}
		}
	}
	
	public static boolean choose(Student nowStudent) {
		int choice = Library.scanner.nextInt();
		switch(choice) {
		case 1:
			Library.searchBookByIdOrName();
			break;
		case 2:
			Library.printAllBooks();
			break;
		case 3:
			nowStudent.print();
			break;
		case 4:
			Library.loanOutToStudent(nowStudent);
			break;
		case 5:
			Library.returnBackFromStudent(nowStudent);
			break;
		case 6:
			System.out.println("| 已退出！感谢您使用本图书借阅系统！");
			return false;
		default:
			System.out.println("| 请选择正确的功能！");
			return true;
		}
		return true;
	}
}
