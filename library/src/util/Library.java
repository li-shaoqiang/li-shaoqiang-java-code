package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


import model.Book;
import model.Student;
import ui.Ui;

/**
 * 
 * @author Li
 *
 */

public class Library {
	private String libraryiName;
	public static List<Book> bookShelf = new ArrayList<>();
	public static List<Student> studentBookList = new ArrayList<>();
	public static Scanner scanner = new Scanner(System.in); 
	public String getLibraryiName() {
		return libraryiName;
	}
	public void setLibraryiName() {
		this.libraryiName = "藏书阁";
	}
	public static List<Book> getBookShelf() {
		return bookShelf;
	}
	public static void setBookShelf(List<Book> bookShelf) {
		Library.bookShelf = bookShelf;
	}
	
	
	
	static {
		Book book1 = new Book("数学之美", "吴军", "978-7-115-37355-7", "49元", "5", "计算机类");
		Book book2 = new Book("硅谷之谜", "吴军", "978-7-115-41092-4", "59元", "5", "计算机类");
		Book book3 = new Book("离散数学及应用", "刘铎", "978-7-302-49663-2", "59元", "10", "计算机类");
		Book book4 = new Book("统计之美", "李舰 海恩", "978-7-121-35404-5", "59元", "5", "计算机类");
		Book book5 = new Book("GO程序设计语言", "艾伦A.A.多诺万 布莱恩W.柯尼汉", "978-7-111-55842-2", "79元", "5", "计算机类");
		bookShelf.add(book1);
		bookShelf.add(book2);
		bookShelf.add(book3);
		bookShelf.add(book4);
		bookShelf.add(book5);
	}

	public static Student userLoginCheck() {
		String userCardId="";
		String password="";
		Student nowStudent = new Student();
		boolean flag = true;
		System.out.println("**************************>>>>欢迎光临藏书阁<<<<**************************");
		System.out.println("|                                                                       |");
		while(true) {
			System.out.println("| 请输入您的借书卡号：");
			userCardId = scanner.nextLine();
			//System.out.println();
			if((userCardId.length() == 0)) {
				System.out.println("| 错误：请填写完所有必填项！！！");continue;
			}
			System.out.println("| 请输入您的登录密码：");
			password = scanner.nextLine();
			System.out.println();
			System.out.println("|                                                                       |");
			System.out.println("|-----------------------------------------------------------------------|");
			if((password.length() == 0)) {
				System.out.println("| 错误：请填写完所有必填项！！！");continue;
			}
			try {
				String fileName = "students.txt";
				FileReader fReader = new FileReader(fileName);
				BufferedReader bfReader = new BufferedReader(fReader);
				String userInformation;
				while((userInformation = bfReader.readLine())!=null) {
					if("".equals(userInformation)) {
						break;
					}
					String[] strings = userInformation.split("\\s+");
					nowStudent.setCardId(strings[0]);
					nowStudent.setStudentName(strings[2]);
					nowStudent.setGender(strings[3]);
					nowStudent.setMaxLoanableNumber(Integer.parseInt(strings[4]));
					nowStudent.setBorrowedNumber(Integer.parseInt(strings[5]));
					if(strings.length==0) {
						break;
					}
					if(userCardId.equals(strings[0])&&password.equals(strings[1])) {
						System.out.println("| 登入成功！                                                            ");
						System.out.println("|                                                                       |");
						System.out.println("|-----------------------------------------------------------------------|");
					    return nowStudent;	
					}
					else if(userCardId.equals(strings[0])&&password.equals(strings[1])) {
						System.out.println("| 账号或密码错误！请重新输入！                                           ");
						System.out.println("|                                                                       |");
						System.out.println("|-----------------------------------------------------------------------|");
						flag=false;
					}
				}
				bfReader.close();
				fReader.close();
				if(flag) {
					System.out.println("| 用户不存在！请重新输入！                                                ");
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void searchBookByIdOrName() {
        boolean flag=true;
        String choice;
        Book targetBook = new Book();
        while(flag) {
        	System.out.println("| 请输入您要查的书籍的名称或ID号：");
        	String id = scanner.next();
        	targetBook = search(id,Library.bookShelf);
        	if(targetBook.getBookName().equals("no information"))
        	{
        		System.out.println("| 查无此书！");
        	}
        	else {
        		System.out.println("| 您所查的书籍信息如下：");
        		targetBook.print();
        	}
        	System.out.println("| 是否继续查询(Y/N):");
        	choice = scanner.next();
        	if(choice.equals("N")) {
        		flag = false;
        		System.out.println("| 查询结束！您选择退出！");
        		break;
        	}
        }
    }
	
    public static Book search(String id,List<Book> bookshelf) {
    	Book targetBook = new Book();
    	Book nullBook  = new Book();
    	Iterator<Book> it = bookshelf.iterator();
    	while(it.hasNext()) {
    		targetBook=it.next();
    		if(targetBook.getBookCode().equals(id)||targetBook.getBookName().equals(id)) {
    			return targetBook;
    		}
    	}
		return nullBook;
    }
    
    public static int searchInBookshelf(String bookName) { 
    	Iterator<Book> it = Library.getBookShelf().iterator();
		Book targetBook = new Book();
		int n = 0;
		while (it.hasNext()) {
			targetBook = it.next();
			if (targetBook.getBookName().equals(bookName)) {
				break;
			}
			n++;
		}
		return n;
    }
    
    public static void printAllBooks() {
    	Iterator<Book> it = bookShelf.iterator();
    	Book book = new Book();
    	while(it.hasNext()) {
    		book = it.next();
    		book.print();
    	}
    }
    
    public static String getBorrowTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return format.format(date);
	}
    
    public static String getReturnTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(" HH:mm");
		int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));
		int month = Integer.parseInt(new SimpleDateFormat("MM").format(date)) + 1;
		int day = Integer.parseInt(new SimpleDateFormat("dd").format(date));
		int monthCount = 13;
		int monthCount1 = 2;
		int dayNumber = 28;
		if (month == monthCount) {
			month = 1;
			year += 1;
		}

		if (day > dayNumber) {
			boolean flag = false;
			flag = year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
			boolean flag2 = false;
			flag2 = (month == 4 || month == 6 || month == 9 || month == 11) && day == 31;
			if (month == monthCount1) {
				if (flag) {
					day = 29;
				} else {
					day = 28;
				}
			} else if (flag2) {
				day = 30;
			}
		}
		String y = year + "";
		String m = "";
		String d = "";
		int monthCount2 = 9;
		int dayCount = 10;
		if (month <= monthCount2) {
			m = "0" + month;
		} else {
			m = month + "";
		}

		if (day < dayCount) {
			d = "0" + day;
		} else {
			d = day + "";
		}

		return y + "-" + m + "-" + d + format.format(date);
	}
    
    public static void loanOutToStudent(Student nowStudent) {
    	boolean flag=true;
        String bookName;
        while(flag) {
        	System.out.println("| 请输入要借的图书名称或ISBN码：");
        	bookName = scanner.next();
        	flag = nowStudent.borrowOneBook(bookName);
        }
    }
    
    public static void returnBackFromStudent(Student nowStudent) {
    	boolean flag=true;
        String bookName;
        while(flag) {
        	System.out.println("| 你已借的书目如下：");
			nowStudent.printMyBookList();
        	System.out.println("| 请输入要还的图书名称：");
        	bookName = scanner.next();
        	flag = nowStudent.returnOneBook(bookName);
        }
    }
}
