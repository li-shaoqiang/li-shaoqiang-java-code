package ui;

/**
 * 
 * @author Li
 *
 */

public class Ui {
	public static void menu() {
		System.out.println("**************************>>>>成功进入藏书阁<<<<**************************");
		System.out.println("|                                                                       ");
		System.out.println("|                              (1)查询书籍                              ");
		System.out.println("|                              (2)查询所有书籍                          ");
		System.out.println("|                              (3)查看个人信息                          ");
		System.out.println("|                              (4)借书                                  ");
		System.out.println("|                              (5)还书                                  ");
		System.out.println("|                              (6)退出                                  ");
		System.out.println("|                                                                       ");
		System.out.println("|***********************************************************************");
	}
}
